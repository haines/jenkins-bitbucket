# Jenkins – Bitbucket integration

![in-action.png](https://bitbucket.org/repo/jBxAB7/images/1696183670-in-action.png)


## How it works

Hook this into your Jenkins job by running `before-build.py` at the very start of the job, and `after-build.py` at the very end. The scripts need the following environment variables:

```
BITBUCKET_REPOSITORY=team-or-username/repo-name
BITBUCKET_USERNAME=jenkins-user-with-read-access-to-the-repo
BITBUCKET_PASSWORD=letmein
```

To inject the build status into the after-build script, I use a hacky workaround involving

* [EnvInject](https://wiki.jenkins-ci.org/display/JENKINS/EnvInject+Plugin),
* [Conditional BuildStep](https://wiki.jenkins-ci.org/display/JENKINS/Conditional+BuildStep+Plugin), and
* [PostBuildScript](https://wiki.jenkins-ci.org/display/JENKINS/PostBuildScript+Plugin).

The build needs to be parameterised by branch name and commit id. I also inject the short id for use in the build name (this could probably be refactored to use a dynamic parameter). I automate this by setting up a second job that polls Git (or, ideally, acts on Bitbucket's post-receive hook) and triggers builds when branches have changed.

## Setup

An example job (config.xml is provided) is shown below.

![config.png](https://bitbucket.org/repo/jBxAB7/images/2095132190-config.png)