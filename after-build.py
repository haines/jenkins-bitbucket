#!/usr/bin/env python

import sys
from pullrequest import after_build, NoSuchPullRequest

branch_name = sys.argv[1]
commit_hash = sys.argv[2]
build_url = sys.argv[3]
success = sys.argv[4] == 'true'

try:
    after_build(branch_name, commit_hash, build_url, success)
except NoSuchPullRequest:
    print 'No pull request found for branch %s' % branch_name
