#!/usr/bin/env python

import sys
from pullrequest import before_build, NoSuchPullRequest

branch_name = sys.argv[1]
commit_hash = sys.argv[2]
build_url = sys.argv[3]

try:
    before_build(branch_name, commit_hash, build_url)
except NoSuchPullRequest:
    print 'No pull request found for branch %s' % branch_name
