import os, json, urllib2, base64

repository = os.environ['BITBUCKET_REPOSITORY']
credentials = '%s:%s' % (os.environ['BITBUCKET_USERNAME'], os.environ['BITBUCKET_PASSWORD'])


class NoSuchPullRequest(Exception):
    pass


def api2_url(path):
    return 'https://bitbucket.org/api/2.0/repositories/%s/pullrequests%s' % (repository, path)


def api1_url(path):
    return 'https://api.bitbucket.org/1.0/repositories/%s/pullrequests%s' % (repository, path)


def send(method, url, data=None):
    request = urllib2.Request(url, data)
    request.add_header('Authorization', 'Basic %s' % base64.standard_b64encode(credentials))
    request.get_method = lambda: method

    response = urllib2.urlopen(request).read()
    if response:
        return json.loads(response)


def find_pull_request(branch_name):
    pull_requests = send('GET', api2_url('/?pagelen=50&state=OPEN'))['values']
    try:
        return next(pull_request for pull_request in pull_requests if is_from_branch(branch_name, pull_request))
    except StopIteration:
        raise NoSuchPullRequest()


def is_from_branch(branch_name, pull_request):
    return branch_name == pull_request['source']['branch']['name']


def is_latest_commit(commit_hash, pull_request):
    return commit_hash.startswith(pull_request['source']['commit']['hash'])


def approve_pull_request(pull_request):
    try:
        send('POST', api2_url('/%s/approve' % pull_request['id']))
    except urllib2.HTTPError, error:
        # swallow 409, it just means it's already been approved
        if error.code != 409:
            raise


def unapprove_pull_request(pull_request):
    try:
        send('DELETE', api2_url('/%s/approve' % pull_request['id']))
    except urllib2.HTTPError, error:
        # swallow 404, it just means it's already been unapproved
        if error.code != 404:
            raise


def comment_on_pull_request(pull_request, comment_text):
    send('POST', api1_url('/%s/comments' % pull_request['id']), 'content=%s' % comment_text)


def before_build(branch_name, commit_hash, build_url):
    pull_request = find_pull_request(branch_name)

    unapprove_pull_request(pull_request)

    comment_on_pull_request(pull_request, '[Started build](%s) for commit %s' % (build_url, abbreviate(commit_hash)))


def after_build(branch_name, commit_hash, build_url, success):
    pull_request = find_pull_request(branch_name)

    if is_latest_commit(commit_hash, pull_request):
        if success:
            approve_pull_request(pull_request)
        else:
            unapprove_pull_request(pull_request)

    comment_on_pull_request(pull_request, '%s [Build %s](%s) for commit %s (%s)' % (build_icon(success), build_status(success), build_url, abbreviate(commit_hash), commit_status(commit_hash, pull_request)))


def build_icon(success):
    if success:
        return ':large_blue_circle:'
    else:
        return ':red_circle:'


def build_status(success):
    if success:
        return 'passed'
    else:
        return 'failed'


def abbreviate(commit_hash):
    return commit_hash[:7]


def commit_status(commit_hash, pull_request):
    if is_latest_commit(commit_hash, pull_request):
        return 'the latest commit'
    else:
        return 'a previous commit'
